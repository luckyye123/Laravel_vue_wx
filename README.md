### 技术栈说明
Laravel5.5.19+Vue2.5.3+Vue-router2.4+Axios+elementUI1.4.9

前端编译：Laravel Mix 后期Webpack打包

### 运行要求
php > 7.0

### 本地测试说明
+ git clone + 本仓库地址

+ composer install // vendor扩展

+ npm install // 扩展node_modules模块

+ composer dump-autoload // 自动加载

+ cp .env.example .env

+ php artisan key:generate

+ php artisan serve // 启动laravel

+ npm run hot / npm run prod // 热加载及线上部署

### 部分截图
![](http://ohl5ggi92.bkt.clouddn.com/weixue_01.png)
![](http://ohl5ggi92.bkt.clouddn.com/weixue_02.png)